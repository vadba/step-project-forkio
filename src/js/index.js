const mobileMenu = document.querySelector('.mobile-menu-icon');
const mobileMenuContent = document.querySelector('.mobile-menu-content');

mobileMenu.addEventListener('click', () => {
    mobileMenu.classList.toggle('active-mobile');
    if (!mobileMenuContent.classList.contains('show')) {
        mobileMenuContent.classList.remove('hide');
        mobileMenuContent.classList.add('show');
    } else {
        mobileMenuContent.classList.remove('show');
        mobileMenuContent.classList.add('hide');
    }
});
